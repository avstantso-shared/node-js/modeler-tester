import type { Model } from '@avstantso/node-or-browser-js--model-core';

import * as Methods from './methods';
import type { Structure } from './structure';
import type { Reads } from './reads';
import type { Writes } from './writes';

export * from './error-expection';

export interface NeedTests extends Reads.NeedTests, Writes.NeedTests {}

export namespace Context {
  export interface Readonly<TModel extends Model.Structure> {
    structure: Structure;
    reads: Reads<TModel>;
  }

  export interface ReadWrite<TModel extends Model.Structure>
    extends Readonly<TModel> {
    writes: Writes<TModel>;
  }

  export type Union<TModel extends Model.Structure> =
    | Readonly<TModel>
    | ReadWrite<TModel>;

  export type Func<
    TModel extends Model.Structure,
    TContext extends Union<TModel>
  > = (callback: (context: TContext) => void) => void;
}

export type Readonly<TModel extends Model.Structure> = Context.Func<
  TModel,
  Context.Readonly<TModel>
>;
export type ReadWrite<TModel extends Model.Structure> = Context.Func<
  TModel,
  Context.ReadWrite<TModel>
>;

export { Methods, Structure, Reads, Writes };
