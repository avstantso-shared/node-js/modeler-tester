import type { Details } from '@avstantso/node-or-browser-js--errors';

export type FailFunc = () => never;

export namespace ExpectedErrorInfo {
  /**
   * @summary expectErrorInfo supported error
   */
  export interface SupportedError {
    details?: Details.Union;

    /**
     * @summary http status, =statusCode
     */
    status?: number;

    /**
     * @summary http status, =status
     */
    statusCode?: number;
  }

  /**
   * @summary expectErrorInfo settings
   */
  export interface Settings {
    synonyms?: { [name: string]: string };
    hasStatus?(e: Error): boolean;
    fail?(): never;
  }
}

export interface ExpectedErrorInfo {
  readonly message: string;
  readonly status?: number;
  expection?(error: Error): void;
  fail?: FailFunc;
}
