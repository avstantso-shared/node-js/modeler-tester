import type { Perform } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type {
  SetupFunc,
  Data,
  PrepareActualData,
  BadRights as BR,
} from './methods';
import type { Structure } from './structure';
import type { NeedTests as CommonNeedTests } from './need-tests';

export namespace Reads {
  export interface NeedTests extends CommonNeedTests {
    readonly readRights?: boolean;
    readonly pagination?: boolean;
  }

  export namespace BadRights {
    export type Select = (params: BR.Params) => void;

    export interface OneByName {
      (name: Data<string>, params: BR.Params): void;
      custom(fieldName: string, name: Data<string>, params: BR.Params): void;
    }

    export type OneById = (id: Data<Model.ID>, params: BR.Params) => void;
  }

  export interface BadRights {
    structure: Structure.Reads.SO;
    select: BadRights.Select;
    oneById: BadRights.OneById;
    oneByName: BadRights.OneByName;
  }

  export namespace GoodRights {
    export namespace Select {
      export type Pages<TModel extends Model.Structure> = <
        TResult = TModel['Select']
      >(
        setRights: SetupFunc,
        pagination: Model.Pagination,
        expectedData: Data<Model.Pagination.Result<{ Select: TResult }>>,
        prepareActual?: PrepareActualData<
          Model.Pagination.Result<TModel>,
          Model.Pagination.Result<{ Select: TResult }>
        >,
        search?: TModel['Condition']
      ) => void;
    }

    export interface Select<TModel extends Model.Structure> {
      <TResult = TModel['Select']>(
        setRights: SetupFunc,
        expectedData: Data<TResult[]>,
        prepareActual?: PrepareActualData<TModel['Select'][], TResult[]>,
        search?: TModel['Condition']
      ): void;
      pages?: Select.Pages<TModel>;
    }
    export namespace OneByName {
      export type Custom<TModel extends Model.Structure> = <
        TResult extends Perform.Able = TModel['Select']
      >(
        fieldName: string,
        name: Data<string>,
        setOtherRights: SetupFunc,
        expectedData: Data<TResult>,
        prepareActual?: PrepareActualData<TModel['Select'], TResult>
      ) => void;
    }

    export interface OneByName<TModel extends Model.Structure> {
      <TResult extends Perform.Able = TModel['Select']>(
        name: Data<string>,
        setOtherRights: SetupFunc,
        expectedData: Data<TResult>,
        prepareActual?: PrepareActualData<TModel['Select'], TResult>
      ): void;
      custom: OneByName.Custom<TModel>;
    }

    export type OneById<TModel extends Model.Structure> = <
      TResult extends Perform.Able = TModel['Select']
    >(
      id: Data<Model.ID>,
      setRights: SetupFunc,
      expectedData: Data<TResult>,
      prepareActual?: PrepareActualData<TModel['Select'], TResult>
    ) => void;
  }

  export interface GoodRights<TModel extends Model.Structure> {
    structure: Structure.Reads.SO;
    select: GoodRights.Select<TModel>;
    oneById: GoodRights.OneById<TModel>;
    oneByName: GoodRights.OneByName<TModel>;
  }
}

export interface Reads<TModel extends Model.Structure> {
  structure: Structure.Reads;
  bad: Reads.BadRights;
  good: Reads.GoodRights<TModel>;
}
