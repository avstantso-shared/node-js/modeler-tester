export namespace Structure {
  export type Callback = () => void;

  export interface External {
    before: Structure.Callback[];
    after: Structure.Callback[];
  }

  export namespace Reads {
    export interface SO extends External {
      select: Callback[];
      one: Callback[];
      oneByName?: Callback[];
    }

    export type BadRights = SO;
    export type GoodRights = SO;
  }

  export interface Reads extends External {
    badRights?: Reads.BadRights;
    goodRights: Reads.GoodRights;
  }

  export namespace Writes {
    export interface IUD extends External {
      insert: Callback[];
      update: Callback[];
      delete: Callback[];
      deleteByName?: Callback[];
    }

    export type BadRights = IUD;

    export interface GoodRights extends External {
      modelErrors: IUD;
      schemaErrors?: IUD;
      normal: IUD;
    }
  }

  export interface Writes extends External {
    badRights: Writes.BadRights;
    goodRights: Writes.GoodRights;
  }
}

export interface Structure extends Structure.External {
  reads: Structure.Reads;
  writes?: Structure.Writes;
}
