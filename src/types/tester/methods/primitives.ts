import type { Perform } from '@avstantso/node-or-browser-js--utils';
export type SetupFunc<T = void> = () => Promise<T>;

export type PrepareActualData<TActual, TResult = TActual> = (
  actual: TActual
) => TResult;

export type Data<TData extends Perform.Able> = Perform.Promise<TData>;
