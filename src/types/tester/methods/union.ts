import type { Model } from '@avstantso/node-or-browser-js--model-core';

import * as RW from './readable-writable';

export namespace Union {
  export type Readable<TModel extends Model.Structure> =
    | RW.Readable<TModel>
    | RW.ReadableNamed<TModel>;

  export type Writable<TModel extends Model.Structure> =
    | RW.ReadableWritable<TModel>
    | RW.ReadableWritableNamed<TModel>;
}

export type Union<TModel extends Model.Structure> =
  | Union.Readable<TModel>
  | Union.Writable<TModel>;
