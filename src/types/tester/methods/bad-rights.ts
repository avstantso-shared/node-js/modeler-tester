import type { ExpectedErrorInfo } from '../error-expection';
import type { SetupFunc } from './primitives';

export namespace BadRights {
  export type Functions = {
    checkNoRights: SetupFunc;
    setOtherRights: SetupFunc;
  };

  export type Params = Functions & {
    expectedErrorInfo: ExpectedErrorInfo;
  };
}
