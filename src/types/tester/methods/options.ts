import type { ExpectedErrorInfo } from '../error-expection';

import { PrepareActualData } from './primitives';

export namespace Options {
  export interface Internal<TResult> extends Options {
    expectedErrorInfo?: ExpectedErrorInfo;
    prepareActual?: PrepareActualData<TResult>;
  }
}

export interface Options {
  access?: string;
}
