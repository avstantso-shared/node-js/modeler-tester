import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Data } from './primitives';
import type { Options } from './options';

export namespace Select {
  export interface Pages<TModel extends Model.Structure> {
    (
      pagination: Model.Pagination,
      search?: TModel['Condition'],
      testerOptions?: Options.Internal<Model.Pagination.Result<TModel>>
    ): Promise<Model.Pagination.Result<TModel>>;
    (
      pagination: Model.Pagination,
      testerOptions?: Options.Internal<Model.Pagination.Result<TModel>>
    ): Promise<Model.Pagination.Result<TModel>>;
  }
}

export interface Select<TModel extends Model.Structure> {
  (
    search?: TModel['Condition'],
    testerOptions?: Options.Internal<TModel['Select'][]>
  ): Promise<TModel['Select'][]>;
  (testerOptions?: Options.Internal<TModel['Select'][]>): Promise<
    TModel['Select'][]
  >;
  pages: Select.Pages<TModel>;
}

export namespace ByName {
  export type Custom<TModel extends Model.Structure> = (
    fieldName: string,
    name: Data<string>,
    testerOptions?: Options.Internal<TModel['Select']>
  ) => Promise<TModel['Select']>;
}

export interface ByName<TModel extends Model.Structure> {
  (
    name: Data<string>,
    testerOptions?: Options.Internal<TModel['Select']>
  ): Promise<TModel['Select']>;
  custom?: ByName.Custom<TModel>;
}

export type ById<TModel extends Model.Structure> = (
  id: Data<Model.ID>,
  testerOptions?: Options.Internal<TModel['Select']>
) => Promise<TModel['Select']>;

export type Insert<TModel extends Model.Structure> = (
  data: Data<TModel['Insert']>,
  testerOptions?: Options.Internal<TModel['Select']>
) => Promise<TModel['Select']>;

export type Update<TModel extends Model.Structure> = (
  data: Data<TModel['Update']>,
  testerOptions?: Options.Internal<TModel['Select']>
) => Promise<TModel['Select']>;

export type Delete = (
  id: Data<Model.ID>,
  testerOptions?: Options.Internal<Model.ID>
) => Promise<Model.ID>;

export namespace DeleteByName {
  export type Custom = (
    fieldName: string,
    name: Data<string>,
    testerOptions?: Options.Internal<Model.ID>
  ) => Promise<Model.ID>;
}

export interface DeleteByName {
  (
    name: Data<string>,
    testerOptions?: Options.Internal<Model.ID>
  ): Promise<Model.ID>;
  custom?: DeleteByName.Custom;
}
