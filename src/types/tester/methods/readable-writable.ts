import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type {
  ById,
  ByName,
  Delete,
  DeleteByName,
  Insert,
  Select,
  Update,
} from './methods';

export interface Readable<TModel extends Model.Structure> {
  select: Select<TModel>;
  byId: ById<TModel>;
}

export interface ReadableNamed<TModel extends Model.Structure>
  extends Readable<TModel> {
  byName: ByName<TModel>;
}

export interface Writable<TModel extends Model.Structure> {
  insert: Insert<TModel>;
  update: Update<TModel>;
  delete: Delete;
}

export interface WritableNamed<TModel extends Model.Structure>
  extends Writable<TModel> {
  deleteByName: DeleteByName;
}

export type ReadableWritable<TModel extends Model.Structure> =
  Readable<TModel> & Writable<TModel>;

export type ReadableWritableNamed<TModel extends Model.Structure> =
  ReadableNamed<TModel> & WritableNamed<TModel>;
