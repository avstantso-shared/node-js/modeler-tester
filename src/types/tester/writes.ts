import type { JSONSchema7 } from 'json-schema';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { ValidatorsIt } from '@avstantso/node-js--model-core-test';
import type { ExpectedErrorInfo } from './error-expection';
import type {
  SetupFunc,
  Data,
  PrepareActualData,
  Options as MehodsOptions,
  BadRights as BR,
} from './methods';
import type { Structure } from './structure';
import type { NeedTests as CommonNeedTests } from './need-tests';

export namespace Writes {
  export interface NeedTests extends CommonNeedTests {
    readonly bySchema?: boolean;
  }

  export namespace BadRights {
    export type Insert<TModel extends Model.Structure> = (
      data: Data<TModel['Insert']>,
      params: BR.Params
    ) => void;

    export type Update<TModel extends Model.Structure> = (
      data: Data<TModel['Update']>,
      params: BR.Params
    ) => void;

    export type Delete = (id: Data<Model.ID>, params: BR.Params) => void;

    export namespace DeleteByName {
      export type Custom = (
        fieldName: string,
        name: Data<string>,
        params: BR.Params
      ) => void;
    }

    export interface DeleteByName {
      (name: Data<string>, params: BR.Params): void;
      custom: DeleteByName.Custom;
    }

    // Insert+Update+Delete+DeleteByName
    export interface Complex<TModel extends Model.Structure> {
      (
        fieldName: string,
        dataInsert: Data<TModel['Insert']>,
        dataUpdate: Data<TModel['Update']>,
        params: BR.Params
      ): void;
      (
        dataInsert: Data<TModel['Insert']>,
        dataUpdate: Data<TModel['Update']>,
        params: BR.Params
      ): void;
    }
  }

  export interface BadRights<TModel extends Model.Structure> {
    structure: Structure.Writes.IUD;
    insert: BadRights.Insert<TModel>;
    update: BadRights.Update<TModel>;
    delete: BadRights.Delete;
    deleteByName: BadRights.DeleteByName;

    complex: BadRights.Complex<TModel>;
  }

  export namespace GoodRights {
    export namespace Errors {
      export interface Options extends MehodsOptions {
        prepare?: SetupFunc;
      }

      export type Insert<TModel extends Model.Structure> = (
        data: Data<TModel['Insert']>,
        expectedErrorInfo: ExpectedErrorInfo,
        options?: Options
      ) => void;

      export type Update<TModel extends Model.Structure> = (
        data: Data<TModel['Update']>,
        expectedErrorInfo: ExpectedErrorInfo,
        options?: Options
      ) => void;

      export type Delete = (
        id: Data<Model.ID>,
        expectedErrorInfo: ExpectedErrorInfo,
        options?: Options
      ) => void;

      export interface DeleteByName {
        (
          name: Data<string>,
          expectedErrorInfo: ExpectedErrorInfo,
          options?: Options
        ): void;
        custom(
          fieldName: string,
          name: Data<string>,
          expectedErrorInfo: ExpectedErrorInfo,
          options?: Options
        ): void;
      }
    }

    export namespace Common {
      export interface Prepares<TModel extends Model.Structure> {
        insert?: SetupFunc;
        update: SetupFunc<TModel['Select']>;
      }

      export namespace FieldInfoTester {
        export type ExpectedErrors = Record<
          'fewer' | 'more' | 'schema',
          ExpectedErrorInfo
        >;

        export interface Prepares<
          TModel extends Model.Structure,
          TResult = TModel['Select']
        > extends Common.Prepares<TModel> {
          actual?: PrepareActualData<TModel['Select'], TResult>;
        }
      }

      export type FieldInfoTester<TModel extends Model.Structure> = <
        TResult = TModel['Select']
      >(
        fieldName: string,
        data: Data<TModel['Insert']>,
        filler: ValidatorsIt.FieldLength.Filler,
        expectedErrorInfos: FieldInfoTester.ExpectedErrors,
        prepares: FieldInfoTester.Prepares<TModel, TResult>
      ) => void;

      export type SchemaRequiredPropsTester<TModel extends Model.Structure> = (
        data: Data<TModel['Insert']>,
        expectedErrorInfo: ExpectedErrorInfo,
        prepares: Prepares<TModel>
      ) => void;
    }

    export namespace ModelError {
      export interface Base<TReturn> {
        structure: Structure.Writes.IUD;

        custom(name: string): TReturn;
      }

      export interface Insert<
        TModel extends Model.Structure,
        TReturn = Errors.Insert<TModel>
      > extends Base<TReturn> {
        alreadyExists: TReturn;
      }

      export interface Update<
        TModel extends Model.Structure,
        TReturn = Errors.Update<TModel>
      > extends Base<TReturn> {
        alreadyExists: TReturn;
        idNotFound: TReturn;
        systemDataChangeForbidden: TReturn;
        accessDenied: TReturn;
        versionConflict: TReturn;
      }

      export interface Delete<TReturn = Errors.Delete> extends Base<TReturn> {
        systemDataChangeForbidden: TReturn;
        accessDenied: TReturn;
      }

      export interface DeleteByName<TReturn = Errors.DeleteByName>
        extends Base<TReturn> {
        systemDataChangeForbidden: TReturn;
        accessDenied: TReturn;
      }
    }

    export interface ModelError<TModel extends Model.Structure> {
      structure: Structure.Writes.IUD;
      insert: ModelError.Insert<TModel>;
      update: ModelError.Update<TModel>;
      delete: ModelError.Delete;
      deleteByName: ModelError.DeleteByName;
    }

    export namespace Normal {
      export type Insert<TModel extends Model.Structure> = <
        TResult = TModel['Select']
      >(
        data: Data<TModel['Insert']>,
        prepareActual?: PrepareActualData<TModel['Select'], TResult>
      ) => void;

      export type Update<TModel extends Model.Structure> = <
        TResult extends TModel['Select'] = TModel['Select']
      >(
        data: Data<TModel['Update']>,
        prepareActual?: PrepareActualData<TModel['Select'], TResult>
      ) => void;

      export type Delete = (
        id: Data<Model.ID>,
        expectedErrorInfo?: ExpectedErrorInfo
      ) => void;

      export interface DeleteByName<TModel extends Model.Structure> {
        (
          prepare: SetupFunc<TModel['Select']>,
          expectedErrorInfo?: ExpectedErrorInfo
        ): void;
        custom(
          fieldName: string,
          prepare: SetupFunc<TModel['Select']>,
          expectedErrorInfo?: ExpectedErrorInfo
        ): void;
      }
    }

    export interface Normal<TModel extends Model.Structure> {
      structure: Structure.Writes.IUD;
      insert: Normal.Insert<TModel>;
      update: Normal.Update<TModel>;
      restore: Normal.Update<TModel>;
      delete: Normal.Delete;
      deleteByName: Normal.DeleteByName<TModel>;
    }
  }

  export interface GoodRights<TModel extends Model.Structure> {
    structure: Structure.Writes.GoodRights;
    fieldInfoTester: GoodRights.Common.FieldInfoTester<TModel>;
    schemaRequiredPropsTester: GoodRights.Common.SchemaRequiredPropsTester<TModel>;
    modelError: GoodRights.ModelError<TModel>;
    normal: GoodRights.Normal<TModel>;
  }
}

export interface Writes<TModel extends Model.Structure> {
  structure: Structure.Writes;
  bad: Writes.BadRights<TModel>;
  good: Writes.GoodRights<TModel>;
}
