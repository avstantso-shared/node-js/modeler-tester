import { DPContext, DPCallback, DescribePool } from './describePool';
import { DEContext, DECallback, DescribeExecutor } from './describeExecutor';

export { DPContext } from './describePool';
export { DEContext } from './describeExecutor';

export namespace Describe {
  export namespace Pool {
    export type Context = DPContext;
    export type Callback = DPCallback;
  }

  export namespace Executor {
    export type Context = DEContext;
    export type Callback = DECallback;
  }
}

export const Describe = {
  Pool: DescribePool,
  Executor: DescribeExecutor,
};
