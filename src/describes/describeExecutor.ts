import 'jest';
import mysql from 'mysql2/promise';
import type { Perform } from '@avstantso/node-or-browser-js--utils';
import { Executor } from '@avstantso/node-js--my-sql-wrapper';
import { DPCallback, DPContext, DescribePool } from './describePool';

export class DEContext extends DPContext {
  executor: Executor;

  constructor(pool: mysql.Pool) {
    super(pool);
    this.executor = Executor({ pool });
    Object.setPrototypeOf(this, DEContext.prototype);
  }
}

export type DECallback = (context: () => DEContext) => void | Promise<void>;

export interface DescribeExecutor {
  (
    database: string,
    name: string | number | Function | jest.FunctionLike,
    callback: DECallback
  ): void | Promise<void>;
  (
    name: string | number | Function | jest.FunctionLike,
    callback: DECallback
  ): void | Promise<void>;
}

export function DescribeExecutor(
  config: Perform<mysql.PoolOptions>
): DescribeExecutor {
  return (...params: any[]): void | Promise<void> => {
    const database: string = params.length > 2 ? params[0] : undefined;
    const name: string | number | Function | jest.FunctionLike =
      params.length > 2 ? params[1] : params[0];
    const callback: DECallback = params.length > 2 ? params[2] : params[1];

    const innerCallback: DPCallback = (context) => {
      return callback(() => new DEContext(context().pool));
    };

    const describePool = DescribePool(config);

    if (database) return describePool(database, name, innerCallback);
    else return describePool(name, innerCallback);
  };
}
