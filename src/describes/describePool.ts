import 'jest';
import mysql from 'mysql2/promise';
import Bluebird from 'bluebird';

import { Perform } from '@avstantso/node-or-browser-js--utils';

export interface PoolInfo {
  pool: mysql.Pool;
  counter: number;
}

export interface Pools {
  [dbname: string]: PoolInfo;
}

const pools: Pools = {};

export class DPContext {
  pool: mysql.Pool;

  constructor(pool: mysql.Pool) {
    this.pool = pool;
    Object.setPrototypeOf(this, DPContext.prototype);
  }
}

export type DPCallback = (context: () => DPContext) => void | Promise<void>;

export interface DescribePool {
  (
    database: string,
    name: string | number | Function | jest.FunctionLike,
    callback: DPCallback
  ): void | Promise<void>;
  (
    name: string | number | Function | jest.FunctionLike,
    callback: DPCallback
  ): void | Promise<void>;
}

export function DescribePool(config: Perform<mysql.PoolOptions>): DescribePool {
  return (...params: any[]): void | Promise<void> => {
    const database: string = params.length > 2 ? params[0] : undefined;
    const name: string | number | Function | jest.FunctionLike =
      params.length > 2 ? params[1] : params[0];
    const callback: DPCallback = params.length > 2 ? params[2] : params[1];

    return describe(name, () => {
      let pool: mysql.Pool;

      beforeAll(async () => {
        const poolInfo = pools[database as keyof Pools];

        if (!poolInfo?.counter) {
          const fullConfig = {
            connectionLimit: 10,
            Promise: Bluebird,
            port: 3306,
            ...Perform(config),
            ...(database ? { database } : {}),
          };

          pool = await mysql.createPool(fullConfig);

          pools[database as keyof Pools] = { pool, counter: 1 };
        } else {
          pool = poolInfo.pool;
          pools[database as keyof Pools] = {
            pool,
            counter: poolInfo.counter + 1,
          };
        }
      });

      afterAll(async () => {
        const poolInfo = pools[database as keyof Pools];
        if (poolInfo.counter <= 1) {
          await pool.end();
          delete pools[database as keyof Pools];
        } else
          pools[database as keyof Pools] = {
            ...poolInfo,
            counter: poolInfo.counter - 1,
          };

        pool = undefined;
      });

      return callback(() => new DPContext(pool));
    });
  };
}
