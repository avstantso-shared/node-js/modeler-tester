import {
  JS,
  OptionalParamsParser,
  Perform,
} from '@avstantso/node-or-browser-js--utils';
import { FIELDS } from '@avstantso/node-or-browser-js--model-core';
import { InspectErrorHelper } from '@avstantso/node-js--jest-helpers';

import { Tester } from '@types';

export async function resolveData<TData extends Perform.Able>(
  data: Tester.Methods.Data<TData>
) {
  return Perform.Promise<TData>(data);
}

export interface ParseOptionalParams {
  fieldName: OptionalParamsParser.ParamsFunc;
}

export function ParseOptionalParams(params: any[]): ParseOptionalParams {
  const fieldName = <TParamsArray extends unknown[]>() =>
    (JS.is.string(params[0])
      ? params
      : [FIELDS.name, ...params]) as TParamsArray;

  return { fieldName };
}

export interface MergeOptions {
  <TResult>(
    options: Tester.Methods.Options[]
  ): Tester.Methods.Options.Internal<TResult>;
  <TResult>(
    options: Tester.Methods.Options,
    expectedErrorInfo: Tester.ExpectedErrorInfo
  ): Tester.Methods.Options.Internal<TResult>;
  <TResult>(
    options: Tester.Methods.Options,
    prepareActual: Tester.Methods.PrepareActualData<TResult>
  ): Tester.Methods.Options.Internal<TResult>;
}

export function isExpectedErrorInfo(
  candidat: unknown
): candidat is Tester.ExpectedErrorInfo {
  return null !== candidat && JS.is.object(candidat) && 'message' in candidat;
}

export function isPrepareActual<TResult>(
  candidat: unknown
): candidat is Tester.Methods.PrepareActualData<TResult> {
  return null !== candidat && JS.is.function(candidat);
}

export function isOptions(
  candidat: unknown
): candidat is Tester.Methods.Options {
  return (
    null !== candidat &&
    JS.is.object(candidat) &&
    !isExpectedErrorInfo(candidat) &&
    !isPrepareActual(candidat)
  );
}

export function isTesterStructureExternal(
  candidat: unknown
): candidat is Tester.Structure.External {
  return (
    null !== candidat &&
    JS.is.object<Tester.Structure.External>(candidat) &&
    'before' in candidat &&
    'after' in candidat
  );
}

export const mergeOptions: MergeOptions = <TResult>(...params: unknown[]) => {
  let r: Tester.Methods.Options.Internal<TResult> = {};

  params.forEach((p) => {
    let o: Tester.Methods.Options.Internal<TResult>;
    if (isExpectedErrorInfo(p)) o = { expectedErrorInfo: p };
    else if (isPrepareActual<TResult>(p)) o = { prepareActual: p };
    else if (isOptions(p)) o = p;
    else return;

    r = { ...r, ...o };
  });

  return r;
};

export namespace TestData {
  export type Init<T> = () => Promise<T> | T;
  export type Final<T> = (data: T) => Promise<any>;

  export interface Overload {
    <T extends Perform.Able>(
      structure: Tester.Structure.External,
      init: Init<T>,
      final?: Final<T>
    ): () => T;
    <T extends Perform.Able>(init: Init<T>, final?: Final<T>): () => T;
  }
}

export const TestData: TestData.Overload = <T extends Perform.Able>(
  ...params: any[]
): (() => T) => {
  const hasStruct = !JS.is.function(params[0]);

  const structure: Tester.Structure.External = hasStruct
    ? params[0]
    : undefined;
  const init: TestData.Init<T> = params[0 + (hasStruct ? 1 : 0)];
  const final: TestData.Final<T> = params[1 + (hasStruct ? 1 : 0)];

  let data: T;

  const f = () => {
    beforeEach(async () => {
      data = await Perform.Promise(init).catch(InspectErrorHelper.report);
    });

    afterEach(async () => {
      final &&
        data &&
        (await Perform.Promise(final, data).catch(InspectErrorHelper.report));
      data = undefined;
    });
  };

  structure ? structure.before.push(f) : f();

  return () => data;
};
