import 'jest';
import { Perform } from '@avstantso/node-or-browser-js--utils';

import { Tester } from '@types';

let settings: Tester.ExpectedErrorInfo.Settings;

function set(aSettings: Tester.ExpectedErrorInfo.Settings) {
  settings = aSettings;
}

function match(errorInfo: Tester.ExpectedErrorInfo) {
  return function (e: Error) {
    if (!errorInfo) throw e;

    // details: To see what could be the problem if it fails
    const { details, status, statusCode } =
      e as Tester.ExpectedErrorInfo.SupportedError;

    const { synonyms, hasStatus } = settings || {};
    const message: string =
      (synonyms && synonyms[e.message as keyof object]) || e.message;

    const hs = (hasStatus && hasStatus(e)) || false;

    if (
      message !== errorInfo.message ||
      (hs && (status || statusCode) !== errorInfo.status)
    )
      throw e;

    expect(1).toBe(1);

    errorInfo.expection && errorInfo.expection(e);
  };
}

function failIfExpected(errorInfo: Tester.ExpectedErrorInfo): never | void {
  if (!errorInfo) return;

  const f = errorInfo.fail || settings?.fail;
  if (f) return f();

  throw Error(`No expected error ${errorInfo.message}`);
}

export function expectErrorInfo<T extends Perform.Able = any>(
  action: Perform.Promise<T>,
  errorInfo: Tester.ExpectedErrorInfo
) {
  return Perform.Promise(action).then(
    () => failIfExpected(errorInfo),
    match(errorInfo)
  );
}

expectErrorInfo.set = set;
expectErrorInfo.match = match;
expectErrorInfo.failIfExpected = failIfExpected;
