import { JS } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

export const Versioner = (
  initial?: number | Model.Versioned
): [
  () => Model.Versioned,
  (increment?: number) => Model.Versioned,
  (value?: number | Model.Versioned) => Model.Versioned
] => {
  let version: number;

  function curVersion(): Model.Versioned {
    return { version };
  }

  function nextVersion(increment: number = 1): Model.Versioned {
    version += increment;
    return { version };
  }

  function setVersion(value?: number | Model.Versioned): Model.Versioned {
    version =
      (JS.is.object<Model.Versioned>(value) ? value.version : value) || 0;
    return { version };
  }

  setVersion(initial);

  return [curVersion, nextVersion, setVersion];
};
