import 'jest';
import { FailedContext } from '@avstantso/node-js--jest-helpers';

import type { Tester } from '@types';

export function Describe<TTestResult>(
  action: string,
  { checkNoRights, setOtherRights }: Tester.Methods.BadRights.Functions,
  test: () => TTestResult
) {
  const tests = {
    'no-rights': checkNoRights,
    'other-rights': setOtherRights,
  };

  let error: any;

  function mkIt([caption, setup]: [string, Tester.Methods.SetupFunc]) {
    it(caption, async () => {
      await setup();

      try {
        return await test();
      } catch (e) {
        error = e;
        throw e;
      }
    });
  }

  describe(action, () => {
    beforeEach(() => {
      error = undefined;
    });

    afterEach(() => {
      if (error)
        FailedContext.report(`Error details: ${JSON.stringify(error)}`);
    });

    Object.entries(tests).forEach(mkIt);
  });
}
