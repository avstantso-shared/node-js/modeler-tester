import 'jest';
import { Cases } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { BadRights } from '@tester-abstract';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Reads.BadRights,
  methods: () => {
    byId: Tester.Methods.ById<TModel>;
    byName: Tester.Methods.ByName<TModel>;
  },
  { readRights: needReadRights, byName: needByName }: Tester.Reads.NeedTests
): [Tester.Reads.BadRights.OneById, Tester.Reads.BadRights.OneByName] {
  const oneBy = (
    structure: Tester.Structure.Callback[],
    fieldName: string,
    { checkNoRights, setOtherRights }: Tester.Methods.BadRights.Functions,
    test: () => Promise<TModel['Select']>
  ) =>
    structure.push(() =>
      BadRights.Describe(
        `one-by-${Cases.kebab(fieldName)}`,
        { checkNoRights, setOtherRights },
        test
      )
    );

  const oneById: Tester.Reads.BadRights.OneById = (
    id,
    { checkNoRights, setOtherRights, expectedErrorInfo }
  ) =>
    needReadRights &&
    oneBy(
      structure.one,
      Model.Fields.id,
      { checkNoRights, setOtherRights },
      () => methods().byId(id, { expectedErrorInfo })
    );

  const oneByName: Tester.Reads.BadRights.OneByName = (
    name,
    { checkNoRights, setOtherRights, expectedErrorInfo }
  ) => {
    if (!(needReadRights && needByName)) return;

    return oneBy(
      structure.oneByName,
      Model.Fields.name,
      { checkNoRights, setOtherRights },
      () => methods().byName(name, { expectedErrorInfo })
    );
  };

  oneByName.custom = (
    fieldName,
    name,
    { checkNoRights, setOtherRights, expectedErrorInfo }
  ) => {
    if (!(needReadRights && needByName)) return;

    return oneBy(
      structure.oneByName,
      fieldName,
      { checkNoRights, setOtherRights },
      () => methods().byName.custom(fieldName, name, { expectedErrorInfo })
    );
  };

  return [oneById, oneByName];
}
