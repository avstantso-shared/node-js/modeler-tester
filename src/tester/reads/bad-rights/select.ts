import 'jest';

import { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { BadRights } from '@tester-abstract';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Reads.BadRights,
  select: () => Tester.Methods.Select<TModel>,
  need: Tester.Reads.NeedTests
): Tester.Reads.BadRights.Select {
  return ({ checkNoRights, setOtherRights, expectedErrorInfo }) =>
    need.readRights &&
    structure.select.push(() => {
      function mkDescribe(test: () => any, pages?: boolean) {
        BadRights.Describe(
          `select${pages ? '-pages' : ''}`,
          { checkNoRights, setOtherRights },
          test
        );
      }

      mkDescribe(() => select()({ expectedErrorInfo }), false);
      need.pagination &&
        mkDescribe(
          () => select().pages(Model.Pagination(0, 1), { expectedErrorInfo }),
          true
        );
    });
}
