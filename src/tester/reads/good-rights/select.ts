import 'jest';
import { Generics } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import { Tester } from '@types';
import { expectEqArrays } from '@expects';
import { resolveData } from '@utils';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Reads.GoodRights,
  select: () => Tester.Methods.Select<TModel>,
  need: Tester.Reads.NeedTests
): Tester.Reads.GoodRights.Select<TModel> {
  const f: Tester.Reads.GoodRights.Select<TModel> = (
    setRights,
    expectedData,
    prepareActual?,
    search?
  ) =>
    structure.select.push(() =>
      it('select', async () => {
        const innerSelect = select();

        const testerOptions: Tester.Methods.Options.Internal<
          TModel['Select'][]
        > = Generics.Cast.To({ prepareActual });

        await setRights();

        const actual = await (search
          ? innerSelect(search, testerOptions)
          : innerSelect(testerOptions));

        const expected = await resolveData(expectedData);

        expectEqArrays(actual, Generics.Cast.To(expected));
      })
    );

  f.pages = (setRights, pagination, expectedData, prepareActual?, search?) =>
    need.pagination &&
    structure.select.push(() =>
      it(`select-pages-${pagination.num}-${pagination.size}`, async () => {
        const innerSelect = select();

        const testerOptions: Tester.Methods.Options.Internal<
          Model.Pagination.Result<TModel>
        > = Generics.Cast.To({ prepareActual });

        await setRights();

        const [actualTotal, actualRows] = await (search
          ? innerSelect.pages(pagination, search, testerOptions)
          : innerSelect.pages(pagination, testerOptions));

        const [expectedTotal, expectedRows] = await resolveData(expectedData);

        expect(actualTotal).toBe(expectedTotal);

        expectEqArrays(actualRows, Generics.Cast.To(expectedRows));
      })
    );

  return f;
}
