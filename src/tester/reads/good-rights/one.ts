import 'jest';
import { Cases, Generics } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { Tester } from '@types';
import { resolveData } from '@utils';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Reads.GoodRights,
  methods: () => {
    byId: Tester.Methods.ById<TModel>;
    byName: Tester.Methods.ByName<TModel>;
  },
  { byName: needByName }: Tester.Reads.NeedTests
): [
  Tester.Reads.GoodRights.OneById<TModel>,
  Tester.Reads.GoodRights.OneByName<TModel>
] {
  const oneBy = (
    structure: Tester.Structure.Callback[],
    fieldName: string,
    setRights: Tester.Methods.SetupFunc,
    expectedData: Tester.Methods.Data<TModel['Select']>,
    method: () => Promise<TModel['Select']>
  ) =>
    structure.push(() =>
      it(`one-by-${Cases.kebab(fieldName)}`, async () => {
        await setRights();

        const actual = await method();
        const expected = await resolveData(expectedData);

        expect(actual).toStrictEqual(expected);
      })
    );

  const oneById: Tester.Reads.GoodRights.OneById<TModel> = (
    id,
    setRights,
    expectedData,
    prepareActual?
  ) =>
    oneBy(
      structure.one,
      Model.Fields.id,
      setRights,
      Generics.Cast.To(expectedData),
      () => methods().byId(id, Generics.Cast.To({ prepareActual }))
    );

  const oneByName: Tester.Reads.GoodRights.OneByName<TModel> = (
    name,
    setRights,
    expectedData,
    prepareActual
  ) => {
    if (!needByName) return;

    return oneBy(
      structure.oneByName,
      Model.Fields.name,
      setRights,
      Generics.Cast.To(expectedData),
      () => methods().byName(name, Generics.Cast.To({ prepareActual }))
    );
  };

  oneByName.custom = (
    fieldName,
    name,
    setRights,
    expectedData,
    prepareActual
  ) => {
    if (!needByName) return;

    return oneBy(
      structure.oneByName,
      fieldName,
      setRights,
      Generics.Cast.To(expectedData),
      () =>
        methods().byName.custom(
          fieldName,
          name,
          Generics.Cast.To({ prepareActual })
        )
    );
  };

  return [oneById, oneByName];
}
