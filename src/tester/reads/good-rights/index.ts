import 'jest';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import Select from './select';
import One from './one';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Reads.GoodRights,
  methods: () => Tester.Methods.ReadableNamed<TModel>,
  needTests: Tester.Reads.NeedTests
): Tester.Reads.GoodRights<TModel> {
  const select = Select(model, structure, () => methods().select, needTests);

  const [oneById, oneByName] = One(model, structure, methods, needTests);

  return { structure, select, oneById, oneByName };
}
