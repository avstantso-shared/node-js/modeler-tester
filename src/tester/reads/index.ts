import 'jest';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import BadRights from './bad-rights';
import GoodRights from './good-rights';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Reads,
  methods: () => Tester.Methods.ReadableNamed<TModel>,
  needTests: Tester.Reads.NeedTests
): Tester.Reads<TModel> {
  const bad = BadRights<TModel>(model, structure.badRights, methods, needTests);
  const good = GoodRights<TModel>(
    model,
    structure.goodRights,
    methods,
    needTests
  );

  return { structure, bad, good };
}
