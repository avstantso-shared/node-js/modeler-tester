import 'jest';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import Reads from './reads';
import Writes from './writes';
import { initStructure, execStructure } from './structure-management';
import { Generics } from '@avstantso/node-or-browser-js--utils';

export interface TesterVariants<TModel extends Model.Structure> {
  Readonly: (
    methods: () => Tester.Methods.Union.Readable<TModel>
  ) => Tester.Readonly<TModel>;
  Simple: (
    methods: () => Tester.Methods.Union.Writable<TModel>
  ) => Tester.ReadWrite<TModel>;
  Hierarchical: (
    methods: () => Tester.Methods.Union.Writable<TModel>
  ) => Tester.ReadWrite<TModel>;
}

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  needTests?: Tester.NeedTests
): TesterVariants<TModel> {
  const make =
    (
      methods: () => Tester.Methods.Union.Writable<TModel>,
      makeWrites?: (structure: Tester.Structure.Writes) => Tester.Writes<TModel>
    ) =>
    (
      callback: (
        context:
          | Tester.Context.Readonly<TModel>
          | Tester.Context.ReadWrite<TModel>
      ) => void
    ) => {
      const structure = initStructure(!makeWrites, needTests);

      const reads = Reads<TModel>(
        model,
        structure.reads,
        Generics.Cast.To(methods),
        needTests
      );
      const writes = makeWrites && makeWrites(structure.writes);

      callback({
        structure,
        reads,
        ...(writes ? { writes } : {}),
      });

      execStructure(structure);
    };

  const Readonly = (methods: () => Tester.Methods.Union.Readable<TModel>) =>
    make(Generics.Cast.To(methods)) as Tester.Readonly<TModel>;

  const Simple = (methods: () => Tester.Methods.Union.Writable<TModel>) =>
    make(methods, (structure) =>
      Writes<TModel>(
        model,
        structure,
        Generics.Cast.To(methods),
        needTests
      ).Simple()
    ) as Tester.ReadWrite<TModel>;

  const Hierarchical = (methods: () => Tester.Methods.Union.Writable<TModel>) =>
    make(methods, (structure) =>
      Writes<TModel>(
        model,
        structure,
        Generics.Cast.To(methods),
        needTests
      ).Hierarchical()
    ) as Tester.ReadWrite<TModel>;

  return { Readonly, Simple, Hierarchical };
}
