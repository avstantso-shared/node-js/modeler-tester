import 'jest';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import BadRights from './bad-rights';
import GoodRights from './good-rights';

export interface WritesVariants<TModel extends Model.Structure> {
  Simple: () => Tester.Writes<TModel>;
  Hierarchical: () => Tester.Writes<TModel>;
}

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes,
  methods: () => Tester.Methods.WritableNamed<TModel>,
  needTests: Tester.Writes.NeedTests
): WritesVariants<TModel> {
  const writes = (
    good: Tester.Writes.GoodRights<TModel>
  ): Tester.Writes<TModel> => ({
    structure,
    bad: BadRights<TModel>(model, structure.badRights, methods, needTests),
    good,
  });

  const grv = GoodRights<TModel>(
    model,
    structure.goodRights,
    methods,
    needTests
  );

  function Simple(): Tester.Writes<TModel> {
    return writes(grv.Simple());
  }

  function Hierarchical(): Tester.Writes<TModel> {
    return writes(grv.Hierarchical());
  }

  return { Simple, Hierarchical };
}
