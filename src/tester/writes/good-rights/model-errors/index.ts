import 'jest';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import Insert from './insert';
import Update from './update';
import Delete from './delete';
import DeleteByName from './deleteByName';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  methods: () => Tester.Methods.WritableNamed<TModel>,
  needTests: Tester.Writes.NeedTests
): Tester.Writes.GoodRights.ModelError<TModel> {
  const insert = Insert<TModel>(model, structure, () => methods().insert);
  const update = Update<TModel>(model, structure, () => methods().update);
  const deleteById = Delete(model, structure, () => methods().delete);
  const deleteByName = DeleteByName(
    model,
    structure,
    () => methods().deleteByName,
    needTests
  );

  return {
    structure,
    insert,
    update,
    delete: deleteById,
    deleteByName,
  };
}
