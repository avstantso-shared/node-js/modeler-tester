import 'jest';
import { Cases } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { InsertError } from '../../-abstract';
import * as C from '../-consts';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  insert: () => Tester.Methods.Insert<TModel>
): Tester.Writes.GoodRights.ModelError.Insert<TModel> {
  const alreadyExists = InsertError(model, structure, insert, C.alreadyExists);

  const custom = (name: string) =>
    InsertError(model, structure, insert, Cases.kebab(name));

  return {
    structure,
    alreadyExists,
    custom,
  };
}
