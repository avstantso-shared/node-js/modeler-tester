export const alreadyExists = 'already-exists';
export const idNotFound = 'id-not-found';
export const systemDataChangeForbidden = 'system-data-change-forbidden';
export const versionConflict = 'version-conflict';
export const accessDenied = 'access-denied';
