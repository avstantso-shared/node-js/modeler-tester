import 'jest';
import { Cases } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { DeleteByNameError } from '../../-abstract';
import * as C from '../-consts';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  deleteByName: () => Tester.Methods.DeleteByName,
  needTests: Tester.Writes.NeedTests
): Tester.Writes.GoodRights.ModelError.DeleteByName {
  const systemDataChangeForbidden = DeleteByNameError(
    model,
    structure,
    deleteByName,
    needTests,
    C.systemDataChangeForbidden
  );
  const accessDenied = DeleteByNameError(
    model,
    structure,
    deleteByName,
    needTests,
    C.accessDenied
  );

  const custom = (name: string) =>
    DeleteByNameError(
      model,
      structure,
      deleteByName,
      needTests,
      Cases.kebab(name)
    );

  return {
    structure,
    systemDataChangeForbidden,
    accessDenied,
    custom,
  };
}
