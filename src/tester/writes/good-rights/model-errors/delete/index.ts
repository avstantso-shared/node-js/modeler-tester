import 'jest';
import { Cases } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { DeleteError } from '../../-abstract';
import * as C from '../-consts';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  deleteById: () => Tester.Methods.Delete
): Tester.Writes.GoodRights.ModelError.Delete {
  const systemDataChangeForbidden = DeleteError(
    model,
    structure,
    deleteById,
    C.systemDataChangeForbidden
  );
  const accessDenied = DeleteError(
    model,
    structure,
    deleteById,
    C.accessDenied
  );

  const custom = (name: string) =>
    DeleteError(model, structure, deleteById, Cases.kebab(name));

  return {
    structure,
    systemDataChangeForbidden,
    accessDenied,
    custom,
  };
}
