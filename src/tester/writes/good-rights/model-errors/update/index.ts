import 'jest';
import { Cases } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { UpdateError } from '../../-abstract';
import * as C from '../-consts';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  update: () => Tester.Methods.Update<TModel>
): Tester.Writes.GoodRights.ModelError.Update<TModel> {
  const alreadyExists = UpdateError(model, structure, update, C.alreadyExists);
  const idNotFound = UpdateError(model, structure, update, C.idNotFound);
  const systemDataChangeForbidden = UpdateError(
    model,
    structure,
    update,
    C.systemDataChangeForbidden
  );
  const accessDenied = UpdateError(model, structure, update, C.accessDenied);
  const versionConflict = UpdateError(
    model,
    structure,
    update,
    C.versionConflict
  );

  const custom = (name: string) =>
    UpdateError(model, structure, update, Cases.kebab(name));

  return {
    structure,
    alreadyExists,
    idNotFound,
    systemDataChangeForbidden,
    accessDenied,
    versionConflict,
    custom,
  };
}
