import 'jest';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { FieldInfoTester, SchemaRequiredProps } from './-common';
import ModelError from './model-errors';
import Normal from './normal';

export interface GoodRightsVariants<TModel extends Model.Structure> {
  Simple: () => Tester.Writes.GoodRights<TModel>;
  Hierarchical: () => Tester.Writes.GoodRights<TModel>;
}

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.GoodRights,
  methods: () => Tester.Methods.WritableNamed<TModel>,
  needTests: Tester.Writes.NeedTests
): GoodRightsVariants<TModel> {
  const good = (
    normal: Tester.Writes.GoodRights.Normal<TModel>
  ): Tester.Writes.GoodRights<TModel> => ({
    structure,
    fieldInfoTester: FieldInfoTester<TModel>(
      model,
      structure,
      methods,
      needTests
    ),
    schemaRequiredPropsTester: SchemaRequiredProps<TModel>(
      model,
      structure.schemaErrors,
      methods,
      needTests
    ),
    modelError: ModelError<TModel>(
      model,
      structure.modelErrors,
      methods,
      needTests
    ),
    normal,
  });

  const nv = Normal<TModel>(model, structure.normal, methods, needTests);

  function Simple(): Tester.Writes.GoodRights<TModel> {
    return good(nv.Simple());
  }

  function Hierarchical(): Tester.Writes.GoodRights<TModel> {
    return good(nv.Hierarchical());
  }

  return { Simple, Hierarchical };
}
