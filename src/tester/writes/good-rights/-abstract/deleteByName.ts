import 'jest';
import { Cases } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { mergeOptions } from '@utils';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  deleteByName: () => Tester.Methods.DeleteByName,
  { byName: needByName }: Tester.Writes.NeedTests,
  name: string
): Tester.Writes.GoodRights.Errors.DeleteByName {
  const f: Tester.Writes.GoodRights.Errors.DeleteByName = (
    nameValue,
    expectedErrorInfo,
    options
  ) => {
    if (!needByName) return;

    structure.deleteByName.push(() =>
      it(`${name}-for-name`, async () => {
        const { prepare, ...rest } = options || {};
        prepare && (await prepare());
        return deleteByName()(nameValue, mergeOptions(rest, expectedErrorInfo));
      })
    );
  };

  f.custom = (fieldName, nameValue, expectedErrorInfo, options) => {
    if (!needByName) return;

    structure.deleteByName.push(() =>
      it(`${name}-for-${Cases.kebab(fieldName)}`, async () => {
        const { prepare, ...rest } = options || {};
        prepare && (await prepare());
        return deleteByName().custom(
          fieldName,
          nameValue,
          mergeOptions(rest, expectedErrorInfo)
        );
      })
    );
  };

  return f;
}
