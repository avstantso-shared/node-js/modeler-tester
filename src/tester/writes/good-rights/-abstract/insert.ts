import 'jest';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { mergeOptions } from '@utils';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  insert: () => Tester.Methods.Insert<TModel>,
  name: string
): Tester.Writes.GoodRights.Errors.Insert<TModel> {
  return (data, expectedErrorInfo, options?) =>
    structure.insert.push(() =>
      it(name, async () => {
        const { prepare, ...rest } = options || {};
        prepare && (await prepare());
        return insert()(data, mergeOptions(rest, expectedErrorInfo));
      })
    );
}
