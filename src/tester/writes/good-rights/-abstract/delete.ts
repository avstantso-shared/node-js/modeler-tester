import 'jest';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { mergeOptions } from '@utils';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  deleteById: () => Tester.Methods.Delete,
  name: string
): Tester.Writes.GoodRights.Errors.Delete {
  return (id, expectedErrorInfo, options?) =>
    structure.delete.push(() =>
      it(name, async () => {
        const { prepare, ...rest } = options || {};
        prepare && (await prepare());
        return deleteById()(id, mergeOptions(rest, expectedErrorInfo));
      })
    );
}
