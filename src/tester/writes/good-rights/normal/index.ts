import 'jest';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import Insert from './insert';
import Update from './update';
import Restore from './restore';
import Delete from './delete';
import DeleteByName from './deleteByName';

export interface NormalVariants<TModel extends Model.Structure> {
  Simple: () => Tester.Writes.GoodRights.Normal<TModel>;
  Hierarchical: () => Tester.Writes.GoodRights.Normal<TModel>;
}

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  methods: () => Tester.Methods.WritableNamed<TModel>,
  needTests: Tester.Writes.NeedTests
): NormalVariants<TModel> {
  const normal = (
    insert: Tester.Writes.GoodRights.Normal.Insert<TModel>,
    update: Tester.Writes.GoodRights.Normal.Update<TModel>,
    restore: Tester.Writes.GoodRights.Normal.Update<TModel>
  ): Tester.Writes.GoodRights.Normal<TModel> => ({
    structure,
    insert,
    update,
    restore,
    delete: Delete(model, structure, () => methods().delete),
    deleteByName: DeleteByName(
      model,
      structure,
      () => methods().deleteByName,
      needTests
    ),
  });

  const iv = Insert<TModel>(model, structure, () => methods().insert);
  const uv = Update<TModel>(model, structure, () => methods().update);
  const rv = Restore<TModel>(model, structure, () => methods().update);

  function Simple(): Tester.Writes.GoodRights.Normal<TModel> {
    return normal(iv.Simple(), uv.Simple(), rv.Simple());
  }

  function Hierarchical(): Tester.Writes.GoodRights.Normal<TModel> {
    return normal(iv.Hierarchical(), uv.Hierarchical(), rv.Hierarchical());
  }

  return { Simple, Hierarchical };
}
