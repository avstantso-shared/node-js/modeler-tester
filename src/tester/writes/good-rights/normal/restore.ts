import 'jest';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { Tester } from '@types';
import { resolveData } from '@utils';

export interface RestoreVariants<TModel extends Model.Structure> {
  Simple: () => Tester.Writes.GoodRights.Normal.Update<TModel>;
  Hierarchical: () => Tester.Writes.GoodRights.Normal.Update<TModel>;
}

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  update: () => Tester.Methods.Update<TModel>
): RestoreVariants<TModel> {
  function Simple(): Tester.Writes.GoodRights.Normal.Update<TModel> {
    return (data, prepareActual) =>
      structure.update.push(() =>
        it('restore', async () => {
          const d = await resolveData(data);

          const a1 = await update()({ ...d, active: false }, { prepareActual });
          expect({
            active: (a1 as Model.Activable).active,
            version: a1.version,
          }).toStrictEqual({ active: false, version: d.version + 1 });

          const a2 = await update()(
            { ...d, active: true, version: d.version + 1 },
            { prepareActual }
          );
          expect({
            active: (a2 as Model.Activable).active,
            version: a2.version,
          }).toStrictEqual({ active: true, version: d.version + 2 });
        })
      );
  }

  function Hierarchical(): Tester.Writes.GoodRights.Normal.Update<TModel> {
    return Simple();
  }

  return { Simple, Hierarchical };
}
