import 'jest';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { Tester } from '@types';
import { resolveData } from '@utils';

const allFieldsUpdated = 'all-fields-updated';

export interface UpdateVariants<TModel extends Model.Structure> {
  Simple: () => Tester.Writes.GoodRights.Normal.Update<TModel>;
  Hierarchical: () => Tester.Writes.GoodRights.Normal.Update<TModel>;
}

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  update: () => Tester.Methods.Update<TModel>
): UpdateVariants<TModel> {
  function Simple(): Tester.Writes.GoodRights.Normal.Update<TModel> {
    return (data, prepareActual) =>
      structure.update.push(() =>
        it(allFieldsUpdated, async () => {
          const d = await resolveData(data);
          return update()(d, { prepareActual }).then((actual) => {
            expect({ ...d, version: actual.version }).toStrictEqual(
              model.Update.Cutter(actual)
            );
          });
        })
      );
  }

  function Hierarchical(): Tester.Writes.GoodRights.Normal.Update<TModel> {
    return Simple();
  }

  return { Simple, Hierarchical };
}
