import 'jest';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { Tester } from '@types';
import { resolveData } from '@utils';
import { Generics } from '@avstantso/node-or-browser-js--utils';

const allFieldsInserted = 'all-fields-inserted';

export interface InsertVariants<TModel extends Model.Structure> {
  Simple: () => Tester.Writes.GoodRights.Normal.Insert<TModel>;
  Hierarchical: () => Tester.Writes.GoodRights.Normal.Insert<TModel>;
}

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  insert: () => Tester.Methods.Insert<TModel>
): InsertVariants<TModel> {
  function Simple(): Tester.Writes.GoodRights.Normal.Insert<TModel> {
    return (data, prepareActual) =>
      structure.insert.push(() =>
        it(allFieldsInserted, async () => {
          const d = await resolveData(data);
          return insert()(d, Generics.Cast.To({ prepareActual })).then(
            (actual) => {
              expect(d).toStrictEqual(model.Insert.Cutter(actual));
            }
          );
        })
      );
  }

  function Hierarchical(): Tester.Writes.GoodRights.Normal.Insert<TModel> {
    return Simple();
  }

  return { Simple, Hierarchical };
}
