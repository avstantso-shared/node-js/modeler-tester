import 'jest';
import { Cases, Generics, JS } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Tester } from '@types';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  deleteByName: () => Tester.Methods.DeleteByName,
  { byName: needByName }: Tester.Writes.NeedTests
): Tester.Writes.GoodRights.Normal.DeleteByName<TModel> {
  const f: Tester.Writes.GoodRights.Normal.DeleteByName<TModel> = (
    prepare,
    expectedErrorInfo?
  ) => {
    if (!needByName) return;

    structure.delete.push(() =>
      it(`delete-by-name`, async () => {
        const data = await prepare();
        const { name } = Generics.Cast.To<Model.Named>(data);

        const delByName = deleteByName();

        await delByName(name).then((delId) => {
          expect(delId).toBe(data.id);
        });

        await delByName(name, expectedErrorInfo && { expectedErrorInfo }).then(
          (delId) => {
            !expectedErrorInfo && expect(delId).toBe(null);
          }
        );
      })
    );
  };

  f.custom = (fieldName, prepare, expectedErrorInfo?) => {
    if (!needByName) return;

    structure.delete.push(() =>
      it(`delete-by-${Cases.kebab(fieldName)}`, async () => {
        const data = await prepare();
        const name: string = JS.get.raw(data, fieldName);

        const delByName = deleteByName();

        await delByName.custom(fieldName, name).then((delId) => {
          expect(delId).toBe(data.id);
        });

        await delByName
          .custom(fieldName, name, expectedErrorInfo && { expectedErrorInfo })
          .then((delId) => {
            !expectedErrorInfo && expect(delId).toBe(null);
          });
      })
    );
  };

  return f;
}
