import 'jest';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { Tester } from '@types';
import { resolveData } from '@utils';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  deleteById: () => Tester.Methods.Delete
): Tester.Writes.GoodRights.Normal.Delete {
  return (idData, expectedErrorInfo?) =>
    structure.delete.push(() =>
      it('delete-by-id', async () => {
        const id = await resolveData(idData);

        const delById = deleteById();

        await delById(id).then((delId) => {
          expect(delId).toBe(id);
        });

        await delById(id, expectedErrorInfo && { expectedErrorInfo }).then(
          (delId) => {
            !expectedErrorInfo && expect(delId).toBe(null);
          }
        );
      })
    );
}
