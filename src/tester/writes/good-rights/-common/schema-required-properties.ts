import 'jest';
import type { JSONSchema7 } from 'json-schema';
import type { ErrorObject } from 'ajv';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import {
  Model,
  resovleSchemaRef,
} from '@avstantso/node-or-browser-js--model-core';

import { Tester } from '@types';
import { resolveData } from '@utils';

const filteredDataErrorStr = (
  filteredData: ErrorObject<string, Record<string, any>, unknown>[]
): string =>
  filteredData.length
    ? `properties ${filteredData
        .map((item) => JSON.stringify(item))
        .join(', ')} errors`
    : '';

const expectRequiredProperties = (
  errData: ErrorObject | ErrorObject[],
  ...propertiesNames: string[]
): void => {
  const dataArr = (
    Array.isArray(errData) ? errData : [errData as ErrorObject]
  ) as ErrorObject[];

  const filteredData = dataArr.filter(
    (item) =>
      !propertiesNames.find((name) => item.params.missingProperty === name)
  );

  const filteredProps = propertiesNames.filter(
    (name) => !dataArr.find((item) => item.params.missingProperty === name)
  );

  if (filteredData.length || filteredProps.length)
    throw Error(
      `Errors: ${filteredDataErrorStr(filteredData)}${
        filteredData.length && filteredProps.length ? ' and ' : ''
      }${
        filteredProps.length
          ? `unexpected requirement for ${filteredProps
              .map((name) => `'${name}'`)
              .join(', ')}`
          : ''
      }`
    );

  return expect(1).toBeTruthy();
};

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.IUD,
  methods: () => Tester.Methods.Writable<TModel>,
  { bySchema: needBySchema }: Tester.Writes.NeedTests
): Tester.Writes.GoodRights.Common.SchemaRequiredPropsTester<TModel> {
  return (data, expectedErrorInfo, prepares) => {
    if (!needBySchema) return;

    const { Insert: schemaI, Update: schemaU } = model.Schema;

    const methodsInfo = [
      {
        dest: structure.insert,
        method: () => methods().insert,
        schema: schemaI,
        prepare: prepares.insert,
      },
      {
        dest: structure.update,
        method: () => methods().update,
        schema: schemaU,
        prepare: prepares.update,
      },
    ];

    methodsInfo.forEach(({ dest, method, schema, prepare }) => {
      const { $ref } = schema;
      if (!$ref)
        throw Error(`"$ref" is empty in schema\r\n${JSON.stringify(schema)}`);

      const subSchema = resovleSchemaRef<JSONSchema7>(schema, $ref);
      const { required } = subSchema;
      if (!required?.length)
        throw Error(
          `"required" is empty in schema\r\n${JSON.stringify(subSchema)}`
        );

      const prepareData = async () => {
        const { id, version } = (
          prepare ? await prepare() : {}
        ) as TModel['Update'];
        const d: Partial<TModel['Update']> = {
          ...(id ? { id, version } : {}),
          ...(await resolveData(data)),
        };
        return d;
      };

      dest.push(() => {
        const partialIt = (without: string[]) => {
          it(`without--${without.join('-')}`, async () => {
            const d = await prepareData();

            without.forEach((field) => delete d[field as keyof object]);

            return method()(Generics.Cast.To(d), {
              expectedErrorInfo: {
                ...expectedErrorInfo,
                expection: (e) => {
                  expectRequiredProperties(
                    Generics.Cast(e).details,
                    ...without
                  );
                  expectedErrorInfo?.expection &&
                    expectedErrorInfo?.expection(e);
                },
              },
            });
          });
        };

        if (1 === required.length) {
          partialIt(required);
          return;
        }

        const halfLength = Math.floor(required.length / 2);
        const r1 = [...required];
        const r2 = r1.splice(halfLength);

        partialIt(r1);
        partialIt(r2);
      });
    });
  };
}
