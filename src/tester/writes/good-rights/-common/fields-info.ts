import 'jest';
import util from 'util';
import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import {
  fillAlphabetStr,
  ajvExpection,
} from '@avstantso/node-js--model-core-test';

import { Tester } from '@types';
import { resolveData } from '@utils';

function noSchemaPath<T extends { schemaPath: string }>(
  data: T
): Omit<T, 'schemaPath'> {
  const { schemaPath, ...rest } = data;
  return rest;
}

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.GoodRights,
  methods: () => Tester.Methods.Writable<TModel>,
  { bySchema: needBySchema }: Tester.Writes.NeedTests
): Tester.Writes.GoodRights.Common.FieldInfoTester<TModel> {
  return (fieldName, data, filler, expectedErrorInfos, prepares) => {
    const fieldInfo =
      model.FieldsInfo && model.FieldsInfo[fieldName as keyof Model.FieldsInfo];

    if (!fieldInfo) {
      console.log(
        'fieldsInfo:',
        util.inspect(model.FieldsInfo, {
          depth: Infinity,
          maxArrayLength: Infinity,
        })
      );
      throw Error(`FieldInfo not found for "${fieldName}"`);
    }

    const methodsInfo = [
      {
        method: () => methods().insert,
        prepare: prepares.insert,
      },
      {
        method: () => methods().update,
        prepare: prepares.update,
      },
    ];
    const methodsWithDest = (...dests: Tester.Structure.Callback[][]) =>
      methodsInfo.map((item, index) => ({ ...item, dest: dests[index] }));

    const prepareAndFillData = async <
      TResult extends TModel['Insert'] | TModel['Update']
    >(
      prepare: Tester.Methods.SetupFunc<any>,
      fieldStrLength: number,
      bad?: boolean
    ) => {
      const { id, version } = (
        prepare ? await prepare() : {}
      ) as TModel['Update'];
      const d: Partial<TModel['Update']> = {
        ...(id ? { id, version } : {}),
        ...(await resolveData(data)),
      };

      JS.set.raw(
        d,
        fieldName,
        filler
          ? filler(fieldName, fieldStrLength, bad)
          : fillAlphabetStr(fieldStrLength)
      );

      return d as TResult;
    };

    const fewerTest =
      (
        prepare: Tester.Methods.SetupFunc<any>,
        method: () =>
          | Tester.Methods.Insert<TModel>
          | Tester.Methods.Update<TModel>,
        expectedErrorInfo: Tester.ExpectedErrorInfo,
        expection?: (error: Error) => void
      ) =>
      async () => {
        const d = await prepareAndFillData(prepare, fieldInfo.length.min - 1);
        return method()(Generics.Cast.To(d), {
          expectedErrorInfo: {
            ...expectedErrorInfo,
            expection:
              expection && expectedErrorInfo.expection
                ? (e) => {
                    expection(e);
                    expectedErrorInfo.expection(e);
                  }
                : expection || expectedErrorInfo.expection,
          },
        });
      };

    const moreTest =
      (
        prepare: Tester.Methods.SetupFunc<any>,
        method: () =>
          | Tester.Methods.Insert<TModel>
          | Tester.Methods.Update<TModel>,
        expectedErrorInfo: Tester.ExpectedErrorInfo,
        expection?: (error: Error) => void
      ) =>
      async () => {
        const d = await prepareAndFillData(prepare, fieldInfo.length.max + 1);
        return method()(Generics.Cast.To(d), {
          expectedErrorInfo: {
            ...expectedErrorInfo,
            expection:
              expection && expectedErrorInfo.expection
                ? (e) => {
                    expection(e);
                    expectedErrorInfo.expection(e);
                  }
                : expection || expectedErrorInfo.expection,
          },
        });
      };

    const normalExpection =
      <TResult>(d: TModel['Insert'] | TModel['Update']) =>
      (fromServer: TResult) => {
        if (Model.Update.Is(fromServer)) {
          if (
            Model.Update.Is(d) &&
            !model.peculiarities?.idCanChangedAfterUpdate
          ) {
            expect(fromServer.id).toBe(d.id);
            expect(fromServer.version).toBeGreaterThan(d.version);
          } else expect(fromServer.id).toBeTruthy();
        } else throw Error('!');
      };

    const minEqTest =
      (
        prepare: Tester.Methods.SetupFunc<any>,
        method: () =>
          | Tester.Methods.Insert<TModel>
          | Tester.Methods.Update<TModel>
      ) =>
      async () => {
        const d: any = await prepareAndFillData(prepare, fieldInfo.length.min);
        return method()(
          d,
          Generics.Cast.To({ prepareActual: prepares.actual })
        ).then(normalExpection(d));
      };

    if (!needBySchema) {
      methodsWithDest(
        structure.modelErrors.insert,
        structure.modelErrors.update
      ).forEach(({ dest, method, prepare }) => {
        const hasMin = JS.is.number(fieldInfo.length?.min);
        const hasMax = JS.is.number(fieldInfo.length?.max);

        if (!(hasMin || hasMax)) return;

        dest.push(() =>
          describe('field-info', () =>
            describe(fieldName, () => {
              hasMin &&
                it(
                  'fewer-min',
                  fewerTest(prepare, method, expectedErrorInfos.fewer)
                );

              hasMax &&
                it(
                  'more-max',
                  moreTest(prepare, method, expectedErrorInfos.more)
                );
            }))
        );
      });
    } else {
      const ajve = ajvExpection(fieldName);

      methodsWithDest(
        structure.schemaErrors.insert,
        structure.schemaErrors.update
      ).forEach(({ dest, method, prepare }) =>
        dest.push(() =>
          describe('field-info', () =>
            describe(fieldName, () => {
              function expectDetails(e: Error, aj: object) {
                const { details } = e as any;
                const actual = noSchemaPath(details[0]);
                const expected = noSchemaPath(Generics.Cast.To(aj));
                expect(actual).toStrictEqual(expected);
              }

              if (fieldInfo.pattern) {
                it('pattern', async () => {
                  const d = await prepareAndFillData(
                    prepare,
                    fieldInfo.length.max,
                    true
                  );
                  return method()(Generics.Cast.To(d), {
                    expectedErrorInfo: {
                      ...expectedErrorInfos.schema,
                      expection: (e) => {
                        expectDetails(e, ajve.pattern(fieldInfo.pattern));
                        expectedErrorInfos.schema?.expection &&
                          expectedErrorInfos.schema.expection(e);
                      },
                    },
                  });
                });
              } else {
                if (JS.is.number(fieldInfo.length?.min))
                  it(
                    'fewer-min',
                    fewerTest(
                      prepare,
                      method,
                      expectedErrorInfos.schema,
                      (e: Error) =>
                        expectDetails(e, ajve.minLength(fieldInfo.length.min))
                    )
                  );

                if (JS.is.number(fieldInfo.length?.max))
                  it(
                    'more-max',
                    moreTest(prepare, method, expectedErrorInfos.schema, (e) =>
                      expectDetails(e, ajve.maxLength(fieldInfo.length.max))
                    )
                  );
              }
            }))
        )
      );
    }

    methodsWithDest(structure.normal.insert, structure.normal.update).forEach(
      ({ dest, method, prepare }) =>
        dest.push(() =>
          describe('field-info', () => {
            if (!needBySchema) {
              if (JS.is.number(fieldInfo.length?.min))
                it(`acceptable-${fieldName}`, minEqTest(prepare, method));

              return;
            }

            describe(fieldName, () => {
              if (fieldInfo.pattern) {
                it('pattern', async () => {
                  const d = await prepareAndFillData(
                    prepare,
                    fieldInfo.length?.max || fieldInfo.length?.min || 10,
                    false
                  );
                  return method()(
                    Generics.Cast.To(d),
                    Generics.Cast.To({
                      prepareActual: prepares.actual,
                    })
                  ).then(normalExpection(d));
                });
              } else {
                if (JS.is.number(fieldInfo.length?.min))
                  it('min-eq', minEqTest(prepare, method));

                if (JS.is.number(fieldInfo.length?.max))
                  it('max-eq', async () => {
                    const d = await prepareAndFillData(
                      prepare,
                      fieldInfo.length.max
                    );
                    return method()(
                      Generics.Cast.To(d),
                      Generics.Cast.To({
                        prepareActual: prepares.actual,
                      })
                    ).then(normalExpection(d));
                  });
              }
            });
          })
        )
    );
  };
}
