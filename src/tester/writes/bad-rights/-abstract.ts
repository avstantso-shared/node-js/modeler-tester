import 'jest';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { BadRights } from '@tester-abstract';

export function badRightsWriteTests<TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  action: string,
  { checkNoRights, setOtherRights }: Tester.Methods.BadRights.Functions,
  method: () => Promise<unknown>
) {
  BadRights.Describe(action, { checkNoRights, setOtherRights }, method);
}
