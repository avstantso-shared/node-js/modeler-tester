import 'jest';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { Tester } from '@types';
import { ParseOptionalParams, resolveData } from '@utils';
import Insert from './insert';
import Update from './update';
import Delete from './delete';
import DeleteByName from './deleteByName';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.BadRights,
  methods: () => Tester.Methods.WritableNamed<TModel>,
  needTests: Tester.Writes.NeedTests
): Tester.Writes.BadRights.Complex<TModel> {
  return (...params: any[]) => {
    const [fieldName, dataInsert, dataUpdate, brParams] =
      ParseOptionalParams(params).fieldName<
        [
          string,
          Tester.Methods.Data<TModel['Insert']>,
          Tester.Methods.Data<TModel['Update']>,
          Tester.Methods.BadRights.Params
        ]
      >();

    const insert = Insert<TModel>(model, structure, () => methods().insert);
    const update = Update<TModel>(model, structure, () => methods().update);
    const deleteById = Delete(model, structure, () => methods().delete);
    const deleteByName = DeleteByName(
      model,
      structure,
      () => methods().deleteByName,
      needTests
    );

    const getId = () => resolveData(dataUpdate).then(({ id }) => id);
    const getName = () =>
      resolveData(dataUpdate).then(
        (updateData) => updateData[fieldName as keyof object]
      );

    insert(dataInsert, brParams);
    update(dataUpdate, brParams);

    deleteById(getId, brParams);

    if (Model.Fields.name === fieldName) deleteByName(getName, brParams);
    else deleteByName.custom(fieldName, getName, brParams);
  };
}
