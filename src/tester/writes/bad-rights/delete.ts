import 'jest';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { badRightsWriteTests } from './-abstract';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.BadRights,
  deleteById: () => Tester.Methods.Delete
): Tester.Writes.BadRights.Delete {
  return (id, { checkNoRights, setOtherRights, expectedErrorInfo }) =>
    structure.delete.push(() =>
      badRightsWriteTests(
        model,
        'delete',
        { checkNoRights, setOtherRights },
        () => deleteById()(id, { expectedErrorInfo })
      )
    );
}
