import 'jest';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { badRightsWriteTests } from './-abstract';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.BadRights,
  insert: () => Tester.Methods.Insert<TModel>
): Tester.Writes.BadRights.Insert<TModel> {
  return (data, { checkNoRights, setOtherRights, expectedErrorInfo }) =>
    structure.insert.push(() =>
      badRightsWriteTests(
        model,
        'insert',
        { checkNoRights, setOtherRights },
        () => insert()(data, { expectedErrorInfo })
      )
    );
}
