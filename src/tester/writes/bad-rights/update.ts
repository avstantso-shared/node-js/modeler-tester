import 'jest';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { badRightsWriteTests } from './-abstract';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.BadRights,
  update: () => Tester.Methods.Update<TModel>
): Tester.Writes.BadRights.Update<TModel> {
  return (data, { checkNoRights, setOtherRights, expectedErrorInfo }) =>
    structure.update.push(() =>
      badRightsWriteTests(
        model,
        'update',
        { checkNoRights, setOtherRights },
        () => update()(data, { expectedErrorInfo })
      )
    );
}
