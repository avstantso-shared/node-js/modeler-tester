import 'jest';
import { Cases } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Tester } from '@types';
import { badRightsWriteTests } from './-abstract';

export default function <TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  structure: Tester.Structure.Writes.BadRights,
  deleteByName: () => Tester.Methods.DeleteByName,
  { byName: needByName }: Tester.Writes.NeedTests
): Tester.Writes.BadRights.DeleteByName {
  const f: Tester.Writes.BadRights.DeleteByName = (
    name,
    { checkNoRights, setOtherRights, expectedErrorInfo }
  ) => {
    if (!needByName) return;

    structure.deleteByName.push(() =>
      badRightsWriteTests(
        model,
        'delete-by-name',
        { checkNoRights, setOtherRights },
        () => deleteByName()(name, { expectedErrorInfo })
      )
    );
  };

  f.custom = (
    fieldName,
    name,
    { checkNoRights, setOtherRights, expectedErrorInfo }
  ) => {
    if (!needByName) return;

    structure.deleteByName.push(() =>
      badRightsWriteTests(
        model,
        `delete-by-${Cases.kebab(fieldName)}`,
        { checkNoRights, setOtherRights },
        () => deleteByName().custom(fieldName, name, { expectedErrorInfo })
      )
    );
  };

  return f;
}
