import 'jest';
import type { Tester } from '@types';

export const initStructure = (
  readOnly: boolean,
  needTests: Tester.NeedTests
): Tester.Structure => {
  const ex = <T extends object>(
    structure: T
  ): T & Tester.Structure.External => ({
    ...structure,
    before: [],
    after: [],
  });

  const initSO = (needByNameTests?: boolean): Tester.Structure.Reads.SO =>
    ex({
      select: [],
      one: [],
      ...(needByNameTests ? { oneByName: [] } : {}),
    });

  const initIUD = (needByNameTests?: boolean): Tester.Structure.Writes.IUD =>
    ex({
      insert: [],
      update: [],
      delete: [],
      ...(needByNameTests ? { deleteByName: [] } : {}),
    });

  const reads: Tester.Structure.Reads = ex({
    ...(needTests.readRights ? { badRights: initSO(needTests.byName) } : {}),
    goodRights: initSO(needTests.byName),
  });

  if (readOnly) return ex({ reads });

  const writes: Tester.Structure.Writes = ex({
    badRights: initIUD(needTests.byName),
    goodRights: ex({
      modelErrors: initIUD(needTests.byName),
      ...(needTests.bySchema
        ? { schemaErrors: initIUD(needTests.byName) }
        : {}),
      normal: initIUD(needTests.byName),
    }),
  });

  return ex({ reads, writes });
};

export const execStructure = (structure: Tester.Structure): void => {
  const exec = (callbacks: Tester.Structure.Callback[]) =>
    callbacks.forEach((callback) => callback());

  const execTreeNode = (
    node: Tester.Structure.Reads.SO | Tester.Structure.Writes.IUD,
    addDescribe?: boolean
  ) => {
    const { before, after, ...rest } = node;

    exec(before);

    Object.keys(rest).forEach((key) => {
      const _exec = () => exec(rest[key as keyof object]);

      addDescribe ? describe(key, _exec) : _exec();
    });

    exec(after);
  };

  const { before: beforeGlobal, reads, writes, after: afterGlobal } = structure;
  exec(beforeGlobal);

  describe('reads', () => {
    const { before, badRights, goodRights, after } = reads;
    exec(before);

    [badRights, goodRights].forEach(
      (rights, index) =>
        rights &&
        describe(`${index ? 'good' : 'bad'}-rights`, () => execTreeNode(rights))
    );

    exec(after);
  });

  if (writes)
    describe('writes', () => {
      const { before, badRights, goodRights, after } = writes;
      exec(before);

      describe('bad-rights', () => execTreeNode(badRights));

      describe('good-rights', () => {
        const {
          modelErrors,
          schemaErrors,
          normal,
          before: beforeWritesGood,
          after: afterWritesGood,
        } = goodRights;
        exec(beforeWritesGood);

        if (schemaErrors)
          describe('schema-errors', () => execTreeNode(schemaErrors, true));

        describe('model-errors', () => execTreeNode(modelErrors, true));

        describe('normal', () => execTreeNode(normal, true));

        exec(afterWritesGood);
      });

      exec(after);
    });

  exec(afterGlobal);
};
